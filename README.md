Viamar Scilla, Toronto, has been in the freight industry since 1976. Specializing in freight and vehicle transportation, we ship used cars, trailers, trucks, motorcycles along with heavy equipment to over 250 international ports across the globe. We are also experienced international movers for families, individuals and businesses. Our freight services are flexible and customizable. Were proud members of the CIFFA (International Freight Forwarders Association) and FIATA (International Federation of Freight Forwarders Associations).

Website: https://www.viamar.ca/
